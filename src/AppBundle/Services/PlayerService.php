<?php

namespace AppBundle\Services;


use AppBundle\Entity\Club;
use AppBundle\Entity\League;
use AppBundle\Entity\Player;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 12:20
 */


class PlayerService
{
	protected $repository, $em;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;
		$this->repository = $entityManager->getRepository(Player::class);
	}

	public function getAllByClub($league_id, $club_id)
	{
		return $this->repository->findBy(array('leagueId' => $league_id, 'clubId' => $club_id));
	}
	
	public function findByColumn($column, $value)
	{
		return $this->repository->findOneBy(array($column => $value));
	}

	public function newPlayer($league_id, $club_id, Request $request)
	{

		$rep = $this->em->getRepository(League::class);
		$league = $rep->find($league_id);

		$rep2 = $this->em->getRepository(Club::class);
		$club = $rep2->find($club_id);

		$player = new Player();
		$player->setLeague($league);
		$player->setClub($club);
		$player->setName($request->get('name'));
		$player->setAge($request->get('age'));
		$player->setNationality($request->get('nationality'));
		$player->setPosition($request->get('position'));
		$player->setWorth($request->get('worth'));
		$this->em->persist($player);
		$this->em->flush();

		return $player;
	}

}