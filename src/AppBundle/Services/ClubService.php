<?php

namespace AppBundle\Services;

use AppBundle\Entity\Club;
use AppBundle\Entity\League;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 12:20
 */


class ClubService
{
	protected $repository, $em;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;
		$this->repository = $entityManager->getRepository(Club::class);
	}

	public function getAllByLeague($league_id)
	{
		return $this->repository->findBy(array('leagueId' => $league_id));
	}
	
	public function findByColumn($column, $value)
	{
		return $this->repository->findOneBy(array($column => $value));
	}

	public function newClub($league_id, Request $request)
	{

		$rep = $this->em->getRepository(League::class);
		$league = $rep->find($league_id);

		$club = new Club();
		$club->setLeague($league);
		$club->setName($request->get('name'));
		$club->setWorth($request->get('worth'));
		$this->em->persist($club);
		$this->em->flush();

		return $club;
	}

}