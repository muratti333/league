<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\League;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 12:20
 */


class LeagueService
{
	protected $repository, $em;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;
		$this->repository = $entityManager->getRepository(League::class);
	}

	public function getAll()
	{
		return $this->repository->findAll();
	}

	public function findByColumn($column, $value)
	{
		return $this->repository->findOneBy(array($column => $value));
	}

	public function newLeague(Request $request)
	{
		$league = new League();
		$league->setName($request->get('name'));
		$league->setWorth($request->get('worth'));
		$this->em->persist($league);
		$this->em->flush();

		return $league;
	}

}