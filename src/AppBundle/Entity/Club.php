<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Club
 *
 * @ORM\Table(name="club")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClubRepository")
 */
class Club
{
    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="club")
     */
    private $players;

    public function __construct()
    {
        $this->players = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="league_id", type="integer")
     */
    private $leagueId;

    /**
     * @var int
     *
     * @ORM\Column(name="worth", type="integer", nullable=true)
     */
    private $worth;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Club
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set leagueId
     *
     * @param integer $leagueId
     *
     * @return Club
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;

        return $this;
    }

    /**
     * Get leagueId
     *
     * @return int
     */
    public function getLeagueId()
    {
        return $this->leagueId;
    }

    /**
     * Set worth
     *
     * @param integer $worth
     *
     * @return Club
     */
    public function setWorth($worth)
    {
        $this->worth = $worth;

        return $this;
    }

    /**
     * Get worth
     *
     * @return int
     */
    public function getWorth()
    {
        return $this->worth;
    }

    /**
     * Get players
     *
     * @return int
     */
    public function getPlayerCount()
    {
        return $this->players->count();
    }

    /**
     * @ORM\ManyToOne(targetEntity="League", inversedBy="clubs")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    private $league;

    public function setLeague($league)
    {
        $this->league = $league;
    }

}

