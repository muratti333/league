<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * League
 *
 * @ORM\Table(name="league")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeagueRepository")
 */
class League
{
    /**
     * @ORM\OneToMany(targetEntity="Club", mappedBy="league")
     */

    private $clubs;

    public function __construct()
    {
        $this->clubs = new ArrayCollection();
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="worth", type="integer", nullable=true)
     */
    private $worth;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return League
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set worth
     *
     * @param integer $worth
     *
     * @return League
     */
    public function setWorth($worth)
    {
        $this->worth = $worth;

        return $this;
    }

    /**
     * Get worth
     *
     * @return int
     */
    public function getWorth()
    {
        return $this->worth;
    }
    
    /**
     * Get clubs
     *
     * @return int
     */
    public function getClubCount()
    {
        return $this->clubs->count();
    }
    
    /**
     * Get players
     *
     * @return int
     */
    public function getPlayerCount()
    {
        return $this->clubs->count();
    }


}

