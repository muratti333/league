<?php
/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 16:00
 */

namespace AppBundle\Controller;
use AppBundle\Services\ClubService;
use AppBundle\Services\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\LeagueService;

class PlayerController extends Controller
{
	protected $leagueService, $clubService, $playerService;
	public function __construct(LeagueService $leagueService, ClubService $clubService, PlayerService $playerService)
	{
		$this->clubService = $clubService;
		$this->leagueService = $leagueService;
		$this->playerService = $playerService;
	}

	/**
	 * @Route("/league/{league_id}/clubs/{club_id}/players", name="default_players")
	 */
	public function getPlayers($league_id, $club_id)
	{
		$league = $this->leagueService->findByColumn('id', $league_id);

		$club = $this->clubService->findByColumn('id', $club_id);

		$players = $this->playerService->getAllByClub($league_id, $club_id);

		return $this->render('default/players.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
			'league' => $league,
			'club' => $club,
			'players' => $players
		]);
	}
}