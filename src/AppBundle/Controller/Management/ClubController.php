<?php
/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 14:00
 */

namespace AppBundle\Controller\Management;

use AppBundle\Services\ClubService;
use AppBundle\Services\LeagueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ClubController extends Controller
{
	protected $clubService;

	public function __construct(ClubService $clubService)
	{
		$this->clubService = $clubService;
	}

	/**
	 * @Route("/yonetim/league/{id}/clubs", name="management_clubs")
	 */
	public function getClubs(Request $request, LeagueService $leagueService)
	{
		$clubs = $this->clubService->getAllByLeague($request->get('id'));

		$league = $leagueService->findByColumn('id', $request->get('id'));

		return $this->render('management/clubs.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
			'clubs' => $clubs,
			'league' => $league
		]);
	}
	
	/**
	 * @Route("/yonetim/league/{id}/clubs/add", name="management_club_add")
	 */
	public function newClub($id, Request $request)
	{
		if($request->get('name') != '') {
			$add = $this->clubService->newClub($id, $request);
			if($add) {
				$this->addFlash(
					'notice',
					'Kulüp Başarıyla Kaydedildi.'
				);
				
				return $this->redirectToRoute('management_clubs', ['id' => $request->get('id')]);
			}
		}
	}
}