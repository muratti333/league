<?php
/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 14:23
 */

namespace AppBundle\Controller\Management;
use AppBundle\Services\ClubService;
use AppBundle\Services\LeagueService;
use AppBundle\Services\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class PlayerController extends Controller
{
	protected $playerService;
	
	public function __construct(PlayerService $playerService)
	{
		$this->playerService = $playerService;
	}
	
	/**
	 * @Route("/yonetim/league/{league_id}/clubs/{club_id}/players", name="management_players")
	 */
	public function getPlayers(Request $request, ClubService $clubService, LeagueService $leagueService)
	{
		$club = $clubService->findByColumn('id', $request->get('club_id'));;
		
		$league = $leagueService->findByColumn('id', $request->get('league_id'));
		
		$players = $this->playerService->getAllByClub($request->get('league_id'), $request->get('club_id'));
		
		return $this->render('management/players.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
			'club' => $club,
			'league' => $league,
			'players' => $players
		]);
	}

	/**
	 * @Route("/yonetim/league/{league_id}/clubs/{club_id}/players/add", name="management_player_add")
	 */
	public function newPlayer($league_id, $club_id, Request $request)
	{
		if($request->get('name') != '') {
			$add = $this->playerService->newPlayer($league_id, $club_id, $request);
			if($add) {
				$this->addFlash(
					'notice',
					'Oyuncu Başarıyla Kaydedildi.'
				);

				return $this->redirectToRoute('management_players', ['league_id' => $league_id, 'club_id' => $club_id]);
			}
		}
	}
}