<?php
/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 13:14
 */

namespace AppBundle\Controller\Management;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\LeagueService;
use Symfony\Component\HttpFoundation\Request;

class ManagementController extends Controller
{
	/**
	 * @Route("/yonetim", name="management_index")
	 */
	public function indexAction(Request $request, LeagueService $leagueService)
	{
		$leagues = $leagueService->getAll();
		
		return $this->render('management/index.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
			'leagues' => $leagues
		]);
	}
}