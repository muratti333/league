<?php
namespace AppBundle\Controller\Management;
use AppBundle\Services\LeagueService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 13:12
 */
class LeagueController extends Controller
{
	protected $leagueService;

	public function __construct(LeagueService $leagueService)
	{
		$this->leagueService = $leagueService;
	}

	/**
	 * @Route("/yonetim/league/add", name="management_league_add")
	 */
	public function newLeague(Request $request)
	{
		if($request->get('name') != '') {
			$add = $this->leagueService->newLeague($request);
			if($add) {
				$this->addFlash(
					'notice',
					'Lig Başarıyla Kaydedildi.'
				);

				return $this->redirectToRoute('management_index');
			}
		}
	}
}