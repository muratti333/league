<?php
/**
 * Created by PhpStorm.
 * User: murat
 * Date: 02/02/2019
 * Time: 15:58
 */

namespace AppBundle\Controller;


use AppBundle\Services\ClubService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\LeagueService;

class ClubsController extends Controller
{

	protected $leagueService, $clubService;
	public function __construct(LeagueService $leagueService, ClubService $clubService)
	{
		$this->leagueService = $leagueService;
		$this->clubService = $clubService;
	}
	
	/**
	 * @Route("/league/{league_id}/clubs", name="default_clubs")
	 */
	public function getClubs($league_id)
	{
		$league = $this->leagueService->findByColumn('id', $league_id);

		$clubs = $this->clubService->getAllByLeague($league_id);

		return $this->render('default/clubs.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
			'league' => $league,
			'clubs' => $clubs
		]);
	}
}